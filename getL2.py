from scapy.all import *
from graphviz import Digraph
import math

pcap_file = "chip-pc-restart.pcap"
dot_extra = {'layout': "circo", 'overlap': 'false', 'splines': 'true'}


class L2net_d2d:
    def __init__(self):
        self._traffic = {}

    def add_packet(self, packet):
        if (packet.src, packet.dst) in self._traffic.keys():
            self._traffic[(packet.src, packet.dst)] = self._traffic[(packet.src, packet.dst)] + 1
        else:
            self._traffic[(packet.src, packet.dst)] = 1

    def __repr__(self):
        outstr = ""
        for (src, dst) in self._traffic.keys():
            occ = self._traffic[(src, dst)]
            outstr = outstr + "\n{} -> {} count: {}".format(src, dst, occ)

        return outstr

    def save_graph_dev_to_dev(self, dot_filename="dev2dev.dot", svg_filename="dev2dev.svg"):

        dot = Digraph(comment='device to device communication', graph_attr=dot_extra,
                      format='svg')

        for (src, dst) in self._traffic.keys():
            dot.node(src.replace(':', '-'), src);

        for (src, dst) in sorted(self._traffic.keys()):
            occ = self._traffic[(src, dst)]
            dot.edge(src.replace(':', '-'),
                     dst.replace(':', '-'),
                     penwidth=str(2 + math.log(occ)), tooltip="count: {}".format(occ))

            dot.save(dot_filename)
            dot.render(svg_filename)

class L2net_traf:
    def __init__(self):
        self._traffic = {}

    def add_packet(self, packet):
        if packet.src in self._traffic.keys():
            self._traffic[packet.src]['out'] = self._traffic[packet.src]['out']+1
        else:
            self._traffic[packet.src] = { 'out': 1, 'in': 0 }

        if packet.dst in self._traffic.keys():
            self._traffic[packet.dst]['in'] = self._traffic[packet.dst]['in']+1
        else:
            self._traffic[packet.dst] = { 'out': 0, 'in': 1 }

    def __repr__(self):
        outstr = ""
        for mac in self._traffic.keys():
            count = self._traffic[mac]
            outstr = outstr + "\n{} in: {} out: {} total: {}".format(mac, count['in'], count['out'], count['in']+count['out'])

        return outstr

    def render_graph(self, dot_filename="dev_traf.dot", svg_filename="dev_traf.svg"):

        dot = Digraph(comment='device in/out', graph_attr=dot_extra, format='svg')

        dot.node( 'LAN', 'current subnet');

        for src in sorted(self._traffic.keys()):
            dot.node(src.replace(':', '-'), src);
            count = self._traffic[src]

            if count['in'] > 0:
                dot.edge('LAN', src.replace(':', '-'),
                     penwidth=str(1 + math.log(count['in'])), tooltip="count: {}".format(count['in']))
            if count['out'] > 0:
                dot.edge( src.replace(':', '-'), 'LAN',
                     penwidth=str(1 + math.log(count['out'])), tooltip="count: {}".format(count['out']))

            dot.save(dot_filename)
            dot.render(svg_filename)

# rdpcap comes from scapy and loads in our pcap file
packets = rdpcap(pcap_file)

# Let's iterate through every packet
count = 0
l2n = L2net_d2d()
l2n_tr = L2net_traf()
trafic_dict = {}
for packet in packets:
    l2n.add_packet(packet)
    l2n_tr.add_packet(packet)
l2n.save_graph_dev_to_dev(dot_filename="d2d.dot")
l2n_tr.render_graph()
